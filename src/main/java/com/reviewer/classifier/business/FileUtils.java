package com.reviewer.classifier.business;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {

    public static void writeFile(String content, String path) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path)));
        bufferedWriter.write(content);
        bufferedWriter.close();
    }

    public static String readFile(String path) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
        while (bufferedReader.ready()) {
            result.append(bufferedReader.readLine());
            result.append("\n");
        }
        bufferedReader.close();
        return result.toString();
    }
}
