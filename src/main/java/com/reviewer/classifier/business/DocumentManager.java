package com.reviewer.classifier.business;

import com.reviewer.classifier.beans.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DocumentManager {

    public Set<String> getBagOfWords() {
        return bagOfWords;
    }

    public void setBagOfWords(Set<String> bagOfWords) {
        this.bagOfWords = bagOfWords;
    }

    private Set<String> bagOfWords;

    public DocumentManager() {
        this.bagOfWords = new HashSet<>();
    }

    public List<Data> readData(String combinedFile, List<String> stopWords) {
        List<Data> result = new ArrayList<>();
        for (String line : combinedFile.split("\n")) {
            int id = Integer.parseInt(line.substring(0, line.indexOf(",")));
            String review = line.substring(line.indexOf(",") + 1, line.lastIndexOf(","));
            review = String.join(" ", TextProcessing.lemmatize(review));
            boolean isRelevant = line.endsWith("1");
            Data data = new Data(id, review, isRelevant);
            result.add(data);
        }
        processData(result, stopWords);
        return result;
    }

    private void processData(List<Data> result, List<String> stopWords) {
        for (Data data : result) {
            List<String> processedWords = TextProcessing.removeStopWords(data.getReview(), stopWords);
            data.setProcessedWords(processedWords);
            bagOfWords.addAll(processedWords);
        }
    }

}
