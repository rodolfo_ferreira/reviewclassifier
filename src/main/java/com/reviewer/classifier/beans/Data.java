package com.reviewer.classifier.beans;

import java.util.ArrayList;
import java.util.List;

public class Data {

    private int id;

    private String review;

    private List<String> processedWords;

    private List<Double> tfidfData;

    private boolean relevant;

    public Data(int id, String review, boolean relevant) {
        this.id = id;
        this.review = review;
        this.relevant = relevant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public boolean isRelevant() {
        return relevant;
    }

    public void setRelevant(boolean relevant) {
        this.relevant = relevant;
    }

    public List<String> getProcessedWords() {
        return processedWords;
    }

    public void setProcessedWords(List<String> processedWords) {
        this.processedWords = processedWords;
    }

    public List<Double> getTfidfData() {
        return tfidfData;
    }

    public void setTfidfData(List<Double> tfidfData) {
        this.tfidfData = tfidfData;
    }

    @Override
    public String toString() {
        return getId() + "," + getReview() + "," + (isRelevant() ? "1" : "0");
    }
}
