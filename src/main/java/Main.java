package com.dolphs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        combine();
    }

    private static void combine() throws IOException {
        String fileA = readFile("divergent.csv");
        String fileB = readFile("covergent.csv");
        String fileCombined = fileA + "\n" + fileB;
        writeFile(fileCombined, "combined.csv");

    }

    private static void diff() throws IOException {
        String alunoA = readFile("C:\\Users\\rodol\\Downloads\\feedbackPortugueseDataset - A .csv");
        String alunoB = readFile("C:\\Users\\rodol\\Downloads\\feedbackPortugueseDataset - B.csv");

        String[] linesA = alunoA.split("\n");
        String[] linesB = alunoB.split("\n");

        StringBuilder convergent = new StringBuilder();
        StringBuilder divergent = new StringBuilder();

        if (linesA.length == linesB.length) {
            for (int i = 1; i < linesA.length; i++) {
                String lineA = linesA[i];
                String lineB = linesB[i];
                if ((lineA.endsWith("1") && lineB.endsWith("1")) || (lineA.endsWith("0") && lineB.endsWith("0"))) {
                    convergent.append(lineA);
                    convergent.append("\n");
                } else {
                    divergent.append(lineB.substring(0, lineB.lastIndexOf(",")));
                    divergent.append("\n");
                }
            }
        }
        writeFile(divergent.toString(), "divergent.csv");
        writeFile(convergent.toString(), "covergent.csv");
    }

    private static void writeFile(String content, String path) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path)));
        bufferedWriter.write(content);
        bufferedWriter.close();
    }

    public static String readFile(String path) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
        while (bufferedReader.ready()) {
            result.append(bufferedReader.readLine());
            result.append("\n");
        }
        bufferedReader.close();
        return result.toString();
    }
}
