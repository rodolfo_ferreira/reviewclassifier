import com.reviewer.classifier.beans.Data;
import com.reviewer.classifier.business.DocumentManager;
import com.reviewer.classifier.business.FileUtils;
import com.reviewer.classifier.business.TextProcessing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Main2 {

    public static void main(String[] args) throws IOException {
        String combinedFile = FileUtils.readFile("combined.csv");
        String stopwordsFile = FileUtils.readFile("stopwords.txt");

        List<String> stopwords = Arrays.asList(stopwordsFile.split("\n"));

        DocumentManager documentManager = new DocumentManager();
        List<Data> dataList = documentManager.readData(combinedFile, stopwords);

        List<List<String>> documents = new ArrayList<>();
        for (Data data : dataList) {
            documents.add(data.getProcessedWords());
        }

        int numOfThreads = 100;
        int dataPerThread = dataList.size() / numOfThreads;

        List<Boolean> running = new ArrayList<>(numOfThreads);
        List<List<Data>> instances = new ArrayList<>(numOfThreads);

        for (int i = 0; i < numOfThreads; i++) {
            int start = i * dataPerThread;
            instances.add(dataList.subList(start, start + dataPerThread));
            running.add(false);
        }

        for (int i = 0; i < instances.size(); i++) {
            int finalI = i;
            new Thread(() -> {
                running.set(finalI, true);
                processarDocs(instances.get(finalI), documentManager.getBagOfWords(), documents);
                running.set(finalI, false);
            }).start();
        }

        while(running.stream().anyMatch(x -> x)) {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (Data data : dataList) {
            List<Double> tfidfData = data.getTfidfData();
            for (int i = 0; i < tfidfData.size(); i++) {
                stringBuilder.append(tfidfData.get(i));
                stringBuilder.append(",");
            }
            stringBuilder.append(data.isRelevant() ? "1" : "0");
            stringBuilder.append("\n");
        }

        FileUtils.writeFile(stringBuilder.toString(), "XBoost-stopwords-lema.txt");

    }

    private static void processarDocs(List<Data> dataList, Set<String> bagOfWords, List<List<String>> documents) {
        int i = 1;
        for (Data data : dataList) {
            System.out.printf("Processando documento %d/%d\n", i++, dataList.size());
            List<String> document = data.getProcessedWords();
            List<Double> tfidfValues = new ArrayList<>();
            for (String term : bagOfWords) {
                double value = TextProcessing.tfIdf(document, documents, term);
                tfidfValues.add(value);
            }
            data.setTfidfData(tfidfValues);
        }
    }
}
