import pandas as pd
import string
from collections import Counter
import spacy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import cohen_kappa_score, make_scorer
from xgboost import XGBClassifier

def run_exps_crossvalidation(x: pd.DataFrame ,
             y: pd.DataFrame) -> pd.DataFrame:
    """
    Lightweight script to test many models and find winners
    :param x: values vector
    :param y: target vector
    :return: DataFrame of predictions
    """

    dfs = []
    print("CARREGANDO MODELO")
    models = [
          ('XGB', XGBClassifier())
        ]

    results = []
    names = []
    kappa_scorer = make_scorer(cohen_kappa_score)
    scoring = {
                'accuracy': 'accuracy',
                'precision_weighted': 'precision_weighted',
                'recall_weighted': 'recall_weighted',
                'f1_weighted': 'f1_weighted',
                'kappa' : kappa_scorer
                }
    print("RODANDO")
    for name, model in models:
        print(name)
        kfold = model_selection.KFold(n_splits=10, shuffle=True)
        cv_results = model_selection.cross_validate(model, x, y, cv=kfold, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        this_df = pd.DataFrame(cv_results)
        this_df['model'] = name
        dfs.append(this_df)

    final = pd.concat(dfs, ignore_index=True)
    return final

if __name__ == '__main__':
    data = pd.read_csv("C:\\Users\\rodol\\IdeaProjects\\Lib1\\XBoost-stopwords-only.txt")
    #     # data.columns = boston.feature_names
    data.info()
    X, y = data.iloc[:, :-1], data.iloc[:, -1]
    final = run_exps_crossvalidation(X, y)
    print(final)
    grouped = final[['test_accuracy', 'test_f1_weighted', 'test_kappa']].groupby(final['model'])
    print(grouped.mean())
